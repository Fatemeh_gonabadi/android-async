package com.example.asynctask;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar, loadingBar;
    Button startButton;
    TextView timer;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

    }

    private void initView() {
        progressBar = findViewById(R.id.progress_bar);
        loadingBar = findViewById(R.id.loading_bar);
        startButton = findViewById(R.id.restart);
        timer = findViewById(R.id.timer);

        loadingBar.setMax(200);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startButton.setEnabled(false);

                startProgress();

            }
        });
    }

    private void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
        loadingBar.setVisibility(View.VISIBLE);

        loadingBar.setProgress(0);
        startTimer();
        new ProgressOperation().execute();

    }

    private void startTimer() {
        final CountDownTimer countDownTimer = new CountDownTimer(20000, 1000) {
            @Override
            public void onTick(long l) {
                timer.setText((20 - (int) (l / 1000)) + "");
            }

            @Override
            public void onFinish() {
                timer.setText("0");
            }
        };
        countDownTimer.start();
    }


    private final class ProgressOperation extends AsyncTask<Integer, Integer, String> {

        @Override
        protected String doInBackground(Integer... params) {
            int speed = 200;

            for (int i = 1; i <= 200; i++) {
                try {
                    Thread.sleep(speed);
                    speed = speed - 1;
                    Log.e("speed", speed + "");

                    publishProgress(i);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "Executed";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            loadingBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar.setVisibility(View.INVISIBLE);
            startButton.setEnabled(true);
        }
    }
}